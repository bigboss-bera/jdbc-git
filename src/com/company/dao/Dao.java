package com.company.dao;

import java.sql.*;

public class Dao {
    public static Connection coonection = null;
    public static PreparedStatement statement = null;
    public static ResultSet rs = null;
    static String url = "jdbc:mysql://localhost:3306/testdb";
    static String username = "root";
    static String password = "root";
    static String sql = "SELECT id,username,`password`,birthday FROM `user`";
    /* 连接数据库的方法 */
    public static Connection coon() {
        try {
            // 1.利用反射机制 加载数据库驱动
            Class.forName("com.mysql.cj.jdbc.Driver");
            // 2. 创建连接
            coonection = DriverManager.getConnection(url, username, password);
            // 3. 连接对象创建一个prepareStatement对象，用于执行SQL语句。
            return coonection;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return coonection;
    }

    /* close 释放资源的方法 */
    public static void close() {
        try {
            if (rs != null) {
                rs.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (coonection != null) {
                coonection.close();
            }
            System.out.println("已关闭资源");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
