package com.company.dao.impl;

import com.company.dao.Dao;
import com.company.dao.UserDao;

import java.sql.SQLException;

// 继承 Dao 里面的方法和属性 还要实现 UserDao 接口
public class UserDaoImpl extends Dao implements UserDao {
    @Override
    public void userXing(String sql) {
        coonection = Dao.coon(); // 获得连接数据库的实例对象
        try {
            statement = coonection.prepareStatement(sql);
            // 4. 执行SQL查询 并获取结果
            rs = statement.executeQuery();
            while (rs.next()){ // 迭代器
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String birthday = rs.getString("birthday");
                System.out.println("ID:"+id+"\tpassword:"+password+"\tusername" +
                        ":"+username+"\tbirthday:"+birthday);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Dao.close();
    }
}
